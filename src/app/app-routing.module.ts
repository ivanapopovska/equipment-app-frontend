
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertComponent } from './alert/alert.component';
import { EmployeeEquipmetComponent } from './employee/employee-equipmet/employee-equipmet.component';
import { EmployeeComponent } from './employee/employee.component'
import { ManagementComponent } from './management/management.component';
import { MentorRequestsComponent } from './mentor/mentor-requests/mentor-requests.component';
import { MentorComponent } from './mentor/mentor.component';

const routes: Routes = [
  {path: 'employees', component: EmployeeComponent},
  {path: 'employees/:id', component: EmployeeEquipmetComponent},
  {path: '', redirectTo: 'employees', pathMatch: 'full'},
  {path: 'mentors', component: MentorComponent},
  {path: 'mentors/:id', component: MentorRequestsComponent},
  {path: 'management', component: ManagementComponent},
  {path: 'alert/:id', component: AlertComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],                                                                                                                                                                                                                                                                                                          
  exports: [RouterModule]
})
export class AppRoutingModule { }
