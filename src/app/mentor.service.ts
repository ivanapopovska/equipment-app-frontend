import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Mentor } from './mentor.model';
import { Request } from './request.model';

@Injectable({
  providedIn: 'root'
})
export class MentorService {

  constructor(private httpClient: HttpClient) { }

  getMentorList(): Observable<Mentor[]>{
    return this.httpClient.get<Mentor[]>("http://localhost:8080/api/mentor");
}

getRequestsByMentor(id: number): Observable<Request[]>{
  

  let url = "http://localhost:8080/api/request/" + id;
  return this.httpClient.get<Request[]>(url);
}

deleteRequest(id :number){
  let url = "http://localhost:8080/api/request/delete/" + id;
  return this.httpClient.delete(url);
}

approveRequest(id :number): Observable<Object>{
  console.log("nesto");
  let url = "http://localhost:8080/api/request/" + id;
  return this.httpClient.patch(url, Object);
}
}
