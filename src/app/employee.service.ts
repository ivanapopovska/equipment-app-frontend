import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from './employee.model';
import { Equipment } from './equipment.model';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpClient: HttpClient) { }

  getEmployeeList(): Observable<Employee[]>{
        return this.httpClient.get<Employee[]>("http://localhost:8080/api/employee");
  }

  getEquipmentByEmployee(id: number): Observable<Equipment[]>{
  

    let url = "http://localhost:8080/api/employee/" + id;
    return this.httpClient.get<Equipment[]>(url);
  }

  getEquipment(): Observable<Equipment[]>{
    return this.httpClient.get<Equipment[]>("http://localhost:8080/api/equipment");
  }

  sendRequest(id :number, equipmentId: number): Observable<Object>{
    console.log("nesto");
    let url = "http://localhost:8080/api/request/" + equipmentId +"/" + id;
    return this.httpClient.post(url, Object);
  }
}
