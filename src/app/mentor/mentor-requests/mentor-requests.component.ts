import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { MentorService } from 'src/app/mentor.service';
import { Request } from 'src/app/request.model';

@Component({
  selector: 'app-mentor-requests',
  templateUrl: './mentor-requests.component.html',
  styleUrls: ['./mentor-requests.component.css']
})
export class MentorRequestsComponent implements OnInit {

  requests: Request[];
  id: number;
  constructor(private mentorService: MentorService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
      }
      );
    this.getRequests();
  }

  getRequests(){
     
      this.mentorService.getRequestsByMentor(this.id).subscribe(
        data => { this.requests = data;
        });
  }

  onClickApprove(id: number){  
    console.log("jsdf");
    this.mentorService.approveRequest(id).subscribe( data =>{
      this.getRequests();
    });
  }

  onClickDelete(id: number){
    console.log("edxfgvc");
    this.mentorService.deleteRequest(id).subscribe( data =>{
      this.getRequests();
    });
  
  }
}
