import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mentor } from '../mentor.model';
import { MentorService } from '../mentor.service';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.css']
})
export class MentorComponent implements OnInit {

  mentors: Mentor[];
  constructor(private mentorService: MentorService,
              private router: Router) { }

  ngOnInit(): void {
    this.getMentors();
  }

  getMentors(){
    return this.mentorService.getMentorList().subscribe(
      data =>{
        this.mentors = data;
      });
  }
  onMentorClick(id: number){
      this.router.navigate(['mentors', id]);
  }
}
