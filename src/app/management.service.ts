import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Request } from './request.model';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(private httpClient: HttpClient) { }

  getApprovedRequests(): Observable<Request[]>{
    return this.httpClient.get<Request[]>("http://localhost:8080/api/management");
}
}
