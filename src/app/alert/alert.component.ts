import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent {
  
  id: number;
  
  constructor(private route: ActivatedRoute,
    private router: Router) { }

    ngOnInit(): void {
      this.route.params.subscribe(
        (params: Params) => {
          this.id = +params['id'];
        }
        );
    }

  onClose() {
    this.router.navigate(['employees', this.id]);
  }
}
