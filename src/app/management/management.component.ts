import { Component, OnInit } from '@angular/core';
import { ManagementService } from '../management.service';
import { Request } from '../request.model';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {

  requests: Request[];
  constructor(private managementService: ManagementService) { }

  ngOnInit(): void {
    this.getRequests();
  }

  getRequests(){
    return this.managementService.getApprovedRequests().subscribe(
      data =>{
        this.requests = data;
      });
  }

}
