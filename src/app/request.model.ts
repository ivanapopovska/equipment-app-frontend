export class Request{
    id: number;
    employeeName: string;
    equipmentName: string;
    mentorName: string;
    approved: boolean;
    type: string;
}