import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EmployeeService } from 'src/app/employee.service';
import { Equipment } from 'src/app/equipment.model';

@Component({
  selector: 'app-employee-equipmet',
  templateUrl: './employee-equipmet.component.html',
  styleUrls: ['./employee-equipmet.component.css']
})
export class EmployeeEquipmetComponent implements OnInit {

  id: number;
  equipmentId: number;
  equipment: Equipment[];
  allEquipment: Equipment[];
  output: string;
  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
      }
      );
      this.getEquipmentByEmployee();
      this.getAllEquipment();
  }

  getEquipmentByEmployee(){
     
    this.employeeService.getEquipmentByEmployee(this.id).subscribe(
      data => { this.equipment = data;
      });
}

  getAllEquipment(){

    this.employeeService.getEquipment().subscribe(
      data => { this.allEquipment = data;
      });
  }

  onSubmit(form: NgForm){
    this.equipmentId = +form.value.equipmentId;
    this.employeeService.sendRequest(this.id, this.equipmentId).subscribe( data =>{
      console.log(data)
      this.router.navigate(['alert', this.id]);
    });
    
  }

}
