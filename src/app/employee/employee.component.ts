import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from '../employee.model';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees: Employee[]
  constructor(private employeeService: EmployeeService,
              private router: Router) { }

  ngOnInit(): void {
    this.getEmployees();
  }
  getEmployees(){
    return this.employeeService.getEmployeeList().subscribe(
      data =>{
        this.employees = data;
      });
}
onClickViewEquipment(id: number){
  this.router.navigate(['employees', id]);
}
}
