import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import {AppRoutingModule} from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import {FormsModule} from '@angular/forms';
import { MentorComponent } from './mentor/mentor.component';
import { ManagementComponent } from './management/management.component';
import { MentorRequestsComponent } from './mentor/mentor-requests/mentor-requests.component';
import { EmployeeEquipmetComponent } from './employee/employee-equipmet/employee-equipmet.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    HeaderComponent,
    MentorComponent,
    ManagementComponent,
    MentorRequestsComponent,
    EmployeeEquipmetComponent,
    AlertComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
